﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VendasFormsEF.Model;
namespace VendasFormsEF.Negocio
{
    class ItemVendaNegocio
    {
        public static float CalcularPrecoVenda(ItemVenda Item)
        {
            return Item.Produto.PrecoCompra + (Item.Produto.PrecoCompra * Item.Produto.Markup / 100);
        }

        public static float CalcularSubTotal(ItemVenda Item)
        {
            return Item.Quantidade * Item.Unitario;
        }
    }
}
