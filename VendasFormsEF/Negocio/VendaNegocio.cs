﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VendasFormsEF.Model;
namespace VendasFormsEF.Negocio
{
    class VendaNegocio
    {
        public static float CalcularTotal(ICollection<ItemVenda> Itens)
        {
            float total = 0;
            foreach (ItemVenda x in Itens)
            {
                total += ItemVendaNegocio.CalcularSubTotal(x);
            }
            return total;
        }
    }
}
