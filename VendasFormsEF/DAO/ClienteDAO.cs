﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VendasFormsEF.Model;
using System.Data;

namespace VendasFormsEF.DAO
{
    class ClienteDAO
    {
        public static bool Insert(Cliente cliente)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Clientes.Add(cliente);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Cliente SearchByCpf(Cliente cliente)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                return db.Clientes.FirstOrDefault(x => x.Cpf.Equals(cliente.Cpf));
            }
            catch
            {
                return null;
            }
        }

        public static IOrderedEnumerable<Cliente> SearchByName(Cliente cliente)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                return db.Clientes.Where(x => x.Nome.Contains(cliente.Nome)).ToList().OrderBy(x => x.Nome);
            }
            catch
            {
                return null;
            }
        }

        public static Cliente SearchById(Cliente cliente)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                return db.Clientes.FirstOrDefault(x => x.Id == cliente.Id);
            }
            catch
            {
                return null;
            }
        }

        public static bool Update(Cliente cliente)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Entry(cliente).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
