﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VendasFormsEF.Model;
using System.Data;
namespace VendasFormsEF.DAO
{
    class VendedorDAO
    {
        public static bool Insert(Vendedor vendedor)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Vendedores.Add(vendedor);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Vendedor SearchByCpf(Vendedor vendedor)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                return db.Vendedores.FirstOrDefault(x => x.Cpf.Equals(vendedor.Cpf));
            }
            catch
            {
                return null;
            }
        }

        public static Vendedor SearchById(Vendedor vendedor)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                return db.Vendedores.FirstOrDefault(x => x.Id == x.Id);
            }
            catch
            {
                return null;
            }
        }

        public static IOrderedEnumerable<Vendedor> SearchByNome(Vendedor vendedor)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                return db.Vendedores.Where(x => x.Nome.Contains(vendedor.Nome)).ToList().OrderBy(x => x.Nome);
            }
            catch
            {
                return null;
            }
        }

        public static bool Update(Vendedor vendedor)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Entry(vendedor).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
