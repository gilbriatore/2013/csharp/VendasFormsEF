﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VendasFormsEF.Model;
using VendasFormsEF.DAO;
using VendasFormsEF.Negocio;
using VendasFormsEF.Util;
namespace VendasFormsEF.View
{
    public partial class FrmCadClientes : Form
    {
        public FrmCadClientes()
        {
            InitializeComponent();
        }

        private void HabilitarComponentes()
        {
            grpDados.Enabled = true;
            btnOk.Enabled = true;
            btnCancelar.Enabled = true;
        }

        private void DesabilitarComponentes()
        {
            grpDados.Enabled = false;
            btnOk.Enabled = false;
            btnCancelar.Enabled = false;
        }

        private void MostrarCliente(Cliente cliente)
        {
            txtId.Text = cliente.Id.ToString();
            txtNome.Text = cliente.Nome;
            mskCpf.Text = cliente.Cpf;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {            
            Cliente cliente = new Cliente();
            cliente.Nome = txtNome.Text;
            cliente.Cpf = mskCpf.Text;
            if (!cliente.Nome.Equals("") && !cliente.Cpf.Equals(""))
            {
                if (ClienteNegocio.ValidarCpf(cliente))
                {
                    if (txtId.Text.Equals(""))
                    {
                        if (ClienteDAO.SearchByCpf(cliente) == null)
                        {
                            if (ClienteDAO.Insert(cliente))
                            {
                                cliente = ClienteDAO.SearchById(cliente);
                                MostrarCliente(cliente);
                                MessageBox.Show("Operação bem sucedida.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                DesabilitarComponentes();
                            }
                            else
                            {
                                MessageBox.Show("Operação não realizada.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Cliente já cadastrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        cliente.Id = int.Parse(txtId.Text);
                        cliente = ClienteDAO.SearchById(cliente);
                        cliente.Nome = txtNome.Text;
                        if (ClienteDAO.Update(cliente))
                        {
                            MessageBox.Show("Operação bem sucedida.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            DesabilitarComponentes();
                        }
                        else
                        {
                            MessageBox.Show("Operação não realizada.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }                    
                }
                else
                {
                    MessageBox.Show("CPF inválido.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Preencha todos os campos.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Utilities.ClearFields(this);
            DesabilitarComponentes();
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            Utilities.ClearFields(this);
            HabilitarComponentes();
            txtNome.Focus();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (txtId.Text != "")
            {
                HabilitarComponentes();
                txtNome.Focus();
            }  
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            FrmLocalizarClientes localizar = new FrmLocalizarClientes(this);
            localizar.ShowDialog();
        }
    
    }
}
