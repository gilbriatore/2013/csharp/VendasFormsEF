﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VendasFormsEF.DAO;
using VendasFormsEF.Model;

namespace VendasFormsEF.View
{
    public partial class FrmLocalizarVendedores : Form
    {
        public FrmLocalizarVendedores()
        {
            InitializeComponent();
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            grdVendedores.Rows.Clear();
            Vendedor vendedor = new Vendedor();
            vendedor.Nome = txtNome.Text;
            foreach (Vendedor x in VendedorDAO.SearchByNome(vendedor))
            {
                grdVendedores.Rows.Add(x.Id, x.Nome);
            }
        }
    }
}
