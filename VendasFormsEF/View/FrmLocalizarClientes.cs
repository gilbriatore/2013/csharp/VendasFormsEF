﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VendasFormsEF.Model;
using VendasFormsEF.DAO;

namespace VendasFormsEF.View
{
    public partial class FrmLocalizarClientes : Form
    {
        FrmCadClientes fCli;
        
        
        public FrmLocalizarClientes(Form Form)
        {
            InitializeComponent();
            if (Form.Name.ToString().Equals("FrmCadClientes"))
            {
                fCli = (FrmCadClientes)Form;
            }            
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            grdClientes.Rows.Clear();
            Cliente cliente = new Cliente();
            cliente.Nome = txtNome.Text;
            foreach (Cliente x in ClienteDAO.SearchByName(cliente))
            {
                grdClientes.Rows.Add(x.Id, x.Nome);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (fCli != null)
            {
                try
                {
                    Cliente cliente = new Cliente();
                    cliente.Id = int.Parse(grdClientes.CurrentRow.Cells[0].Value.ToString());
                    cliente = ClienteDAO.SearchById(cliente);
                    fCli.txtId.Text = cliente.Id.ToString();
                    fCli.txtNome.Text = cliente.Nome;
                    fCli.mskCpf.Text = cliente.Cpf;
                    Close();
                }
                catch
                {
                    MessageBox.Show("Cliente inválido.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void grpDados_Enter(object sender, EventArgs e)
        {

        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblNome_Click(object sender, EventArgs e)
        {

        }

        private void grdClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void grpResultados_Enter(object sender, EventArgs e)
        {

        }
    }
}
