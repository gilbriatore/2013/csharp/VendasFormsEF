﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VendasFormsEF.Util;
using VendasFormsEF.Negocio;
using VendasFormsEF.Model;
using VendasFormsEF.DAO;

namespace VendasFormsEF.View
{
    public partial class FrmCadVendedores : Form
    {
        public FrmCadVendedores()
        {
            InitializeComponent();
        }

        private void HabilitarComponentes()
        {
            grpDados.Enabled = true;
            btnOk.Enabled = true;
            btnCancelar.Enabled = true;
        }

        private void DesabilitarComponentes()
        {
            grpDados.Enabled = false;
            btnOk.Enabled = false;
            btnCancelar.Enabled = false;
        }

        private void MostrarVendedor(Vendedor vendedor)
        {
            txtId.Text = vendedor.Id.ToString();
            txtNome.Text = vendedor.Nome;
            mskCpf.Text = vendedor.Cpf;
            txtTaxa.Text = vendedor.Taxa.ToString("N2");
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtTaxa_Leave(object sender, EventArgs e)
        {
            if (txtTaxa.Text.Equals(""))
            {
                txtTaxa.Text = "0";
            }
            if (Utilities.ValidarMonetario(txtTaxa.Text) == null)
            {
                MessageBox.Show("Taxa de comissão inválida.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtTaxa.Focus();
            }
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            Utilities.ClearFields(this);
            HabilitarComponentes();
            txtNome.Focus();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Vendedor vendedor = new Vendedor();
            vendedor.Nome = txtNome.Text;
            vendedor.Cpf = mskCpf.Text;
            if(txtTaxa.Text.Equals(""))
            {
                txtTaxa.Text  = "0";
            }
            vendedor.Taxa = float.Parse(txtTaxa.Text);
            if (!vendedor.Nome.Equals("") && !vendedor.Cpf.Equals(""))
            {
                if (VendedorNegocio.ValidarCpf(vendedor))
                {
                    if (txtId.Text.Equals(""))
                    {
                        if (VendedorDAO.SearchByCpf(vendedor) == null)
                        {
                            if (VendedorDAO.Insert(vendedor))
                            {
                                vendedor = VendedorDAO.SearchById(vendedor);
                                MostrarVendedor(vendedor);
                                MessageBox.Show("Operação bem sucedida.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                DesabilitarComponentes();
                            }
                            else
                            {
                                MessageBox.Show("Operação não realizada.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Vendedor já cadastrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        vendedor.Id = int.Parse(txtId.Text);
                        vendedor = VendedorDAO.SearchById(vendedor);
                        vendedor.Nome = txtNome.Text;
                        if (VendedorDAO.Update(vendedor))
                        {
                            MessageBox.Show("Operação bem sucedida.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            DesabilitarComponentes();
                        }
                        else
                        {
                            MessageBox.Show("Operação não realizada.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("CPF inválido.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Preencha todos os campos.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
